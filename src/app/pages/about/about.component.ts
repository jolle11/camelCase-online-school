import { Component, OnInit } from '@angular/core';

import { latLng, MapOptions, tileLayer, Map, Marker, icon } from 'leaflet';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
    map!: Map;
    mapOptions!: MapOptions;

    constructor() {}

    ngOnInit() {
        this.initializeMapOptions();
    }

    onMapReady(map: Map) {
        this.map = map;
        this.addSampleMarker();
    }

    private initializeMapOptions() {
        this.mapOptions = {
            center: latLng(41.26294, 1.17031),
            zoom: 12,
            layers: [
                tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data © OpenStreetMap contributors',
                }),
            ],
        };
    }

    private addSampleMarker() {
        const marker = new Marker([41.26294, 1.17031]).setIcon(
            icon({
                iconSize: [25, 41],
                iconAnchor: [13, 41],
                iconUrl: 'assets/marker-icon.png',
            })
        );
        marker.addTo(this.map);
    }
}
