import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Course } from '../courses/models/Icourse';
import { CourseRequestService } from '../courses/services/course-request.service';

@Component({
    selector: 'app-enroll',
    templateUrl: './enroll.component.html',
    styleUrls: ['./enroll.component.scss'],
})
export class EnrollComponent implements OnInit {
    enrollForm: FormGroup;
    submitted: boolean = false;
    moreInfo: string = '';
    courseList: Course[] = [];

    constructor(private formBuilder: FormBuilder, private courseRequestService: CourseRequestService) {
        this.enrollForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.pattern(/^(?=[a-zA-Z]).{2,10}$/)]],
            surname: ['', [Validators.required, Validators.pattern(/^(?=[a-zA-Z]).{2,10}$/)]],
            email: ['', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]],
            phone: ['', [Validators.required, Validators.pattern(/^[679]{1}[0-9]{8}$/)]],
            selection: ['', [Validators.required]],
        });
    }

    ngOnInit(): void {
        this.courseRequestService.getCourses().subscribe((data) => {
            const coursesArray = Object.values(data);
            const courseList = coursesArray.map((course) => {
                course = {
                    name: course.course,
                };
                return course;
            });
            this.courseList = courseList;
        });
    }

    onSubmit(): void {
        this.submitted = true;
        if (this.enrollForm.valid) {
            console.log(this.enrollForm.value);
            console.log(this.enrollForm);
            this.enrollForm.reset();
            this.submitted = false;
        } else {
            setTimeout(() => {
                this.submitted = false;
            }, 1000);
        }
    }
}
