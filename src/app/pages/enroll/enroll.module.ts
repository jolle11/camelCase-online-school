import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { EnrollRoutingModule } from './enroll-routing.module';
import { EnrollComponent } from './enroll.component';
import { CourseRequestService } from '../courses/services/course-request.service';

@NgModule({
    declarations: [EnrollComponent],
    imports: [CommonModule, EnrollRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
    providers: [CourseRequestService],
})
export class EnrollModule {}
