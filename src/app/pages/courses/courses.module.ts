import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesComponent } from './courses.component';
import { CourseDetailComponent } from './components/course-detail/course-detail.component';
import { CourseRequestService } from './services/course-request.service';

@NgModule({
    declarations: [CoursesComponent, CourseDetailComponent],
    imports: [CommonModule, CoursesRoutingModule, HttpClientModule, FontAwesomeModule],
    providers: [CourseRequestService],
})
export class CoursesModule {}
