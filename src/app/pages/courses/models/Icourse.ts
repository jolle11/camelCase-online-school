export interface Course {
    id: number;
    name: string;
    description: string;
    image: string;
    start: string;
    end: string;
}
