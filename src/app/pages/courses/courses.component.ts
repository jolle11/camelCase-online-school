import { Component, OnInit } from '@angular/core';

import { Course } from './models/Icourse';
import { CourseRequestService } from './services/course-request.service';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit {
    courseList: Course[] = [];

    constructor(private courseRequestService: CourseRequestService) {}

    ngOnInit(): void {
        this.courseRequestService.getCourses().subscribe((data) => {
            const coursesArray = Object.values(data);
            const courseList = coursesArray.map((course) => {
                course = {
                    id: course.id,
                    name: course.course,
                    image: course.image,
                };
                return course;
            });
            this.courseList = courseList;
        });
    }
}
