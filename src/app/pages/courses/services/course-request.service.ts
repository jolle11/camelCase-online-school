import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const coursesUrl = 'https://retoolapi.dev/cCmEGc/courses';

@Injectable()
export class CourseRequestService {
    constructor(private http: HttpClient) {}
    getCourses() {
        return this.http.get(coursesUrl);
    }
    getCourseDetail(id: string) {
        return this.http.get(coursesUrl + `/${id}`);
    }
}
