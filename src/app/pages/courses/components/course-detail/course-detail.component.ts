import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faLongArrowAltDown } from '@fortawesome/free-solid-svg-icons';

import { CourseRequestService } from '../../services/course-request.service';

@Component({
    selector: 'app-course-detail',
    templateUrl: './course-detail.component.html',
    styleUrls: ['./course-detail.component.scss'],
})
export class CourseDetailComponent implements OnInit {
    courseDetail: any = {};
    params$: any = '';
    courseId: any;
    faLongArrowAltDown = faLongArrowAltDown;

    constructor(private courseRequestService: CourseRequestService, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.params$ = this.route.paramMap.subscribe((params) => {
            this.courseId = params.get('id');
            this.courseRequestService.getCourseDetail(this.courseId).subscribe((data) => {
                this.courseDetail = data;
                return this.courseDetail;
            });
        });
    }
}
