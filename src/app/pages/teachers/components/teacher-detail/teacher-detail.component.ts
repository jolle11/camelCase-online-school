import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TeacherRequestService } from '../../services/teacher-request.service';

@Component({
    selector: 'app-teacher-detail',
    templateUrl: './teacher-detail.component.html',
    styleUrls: ['./teacher-detail.component.scss'],
})
export class TeacherDetailComponent implements OnInit {
    teacherDetail: any = {};
    params$: any = '';
    teacherId: any;

    constructor(private teacherRequestService: TeacherRequestService, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.params$ = this.route.paramMap.subscribe((params) => {
            this.teacherId = params.get('id');
            this.teacherRequestService.getTeacherDetail(this.teacherId).subscribe((data) => {
                this.teacherDetail = data;
                return this.teacherDetail;
            });
        });
    }
}
