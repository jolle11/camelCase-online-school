import { Component, OnInit } from '@angular/core';

import { Teacher } from './models/Iteacher';
import { TeacherRequestService } from './services/teacher-request.service';

@Component({
    selector: 'app-teachers',
    templateUrl: './teachers.component.html',
    styleUrls: ['./teachers.component.scss'],
})
export class TeachersComponent implements OnInit {
    teacherList: Teacher[] = [];

    constructor(private teacherRequestService: TeacherRequestService) {}

    ngOnInit(): void {
        this.teacherRequestService.getTeachers().subscribe((data) => {
            const teachersArray = Object.values(data);
            const teacherList = teachersArray.map((teacher) => {
                teacher = {
                    id: teacher.id,
                    name: teacher.name,
                    surname: teacher.surname,
                    bio: teacher.bio,
                    avatar: teacher.avatar,
                    teaches: teacher.teaches,
                };
                return teacher;
            });
            this.teacherList = teacherList;
            console.log(this.teacherList);
        });
    }
}
