import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeacherDetailComponent } from './components/teacher-detail/teacher-detail.component';
import { TeachersComponent } from './teachers.component';

const routes: Routes = [
    {
        path: '',
        component: TeachersComponent,
    },
    {
        path: ':id',
        component: TeacherDetailComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TeachersRoutingModule {}
