export interface Teacher {
    id: number;
    name: string;
    surname: string;
    bio: string;
    avatar: string;
    teaches: string;
}
