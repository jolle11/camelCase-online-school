import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const teacherUrl = 'https://retoolapi.dev/nObI61/teachers';

@Injectable()
export class TeacherRequestService {
    constructor(private http: HttpClient) {}
    getTeachers() {
        return this.http.get(teacherUrl);
    }
    getTeacherDetail(id: string) {
        return this.http.get(teacherUrl + `/${id}`);
    }
}
