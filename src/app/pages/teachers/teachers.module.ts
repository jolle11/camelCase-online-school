import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { TeachersRoutingModule } from './teachers-routing.module';
import { TeachersComponent } from './teachers.component';
import { TeacherDetailComponent } from './components/teacher-detail/teacher-detail.component';
import { TeacherRequestService } from './services/teacher-request.service';

@NgModule({
    declarations: [TeachersComponent, TeacherDetailComponent],
    imports: [CommonModule, TeachersRoutingModule, HttpClientModule],
    providers: [TeacherRequestService],
})
export class TeachersModule {}
