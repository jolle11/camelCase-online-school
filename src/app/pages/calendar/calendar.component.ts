import { Component, OnInit } from '@angular/core';

import { Course } from '../courses/models/Icourse';
import { CourseRequestService } from '../courses/services/course-request.service';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
    courseList: Course[] = [];

    constructor(private courseRequestService: CourseRequestService) {}

    ngOnInit(): void {
        this.courseRequestService.getCourses().subscribe((data) => {
            const coursesArray = Object.values(data);
            const courseList = coursesArray.map((course) => {
                course = {
                    name: course.course,
                    image: course.image,
                    start: course.start,
                    end: course.end,
                };
                return course;
            });
            this.courseList = courseList;
        });
    }
}
