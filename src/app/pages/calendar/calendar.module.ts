import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { CourseRequestService } from '../courses/services/course-request.service';

@NgModule({
    declarations: [CalendarComponent],
    imports: [CommonModule, CalendarRoutingModule, HttpClientModule],
    providers: [CourseRequestService],
})
export class CalendarModule {}
