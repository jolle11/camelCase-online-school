import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    carouselImages = [
        {
            explore: 'https://images.pexels.com/photos/3975590/pexels-photo-3975590.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            meet: 'https://images.pexels.com/photos/5212362/pexels-photo-5212362.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            enroll: 'https://images.pexels.com/photos/261621/pexels-photo-261621.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
        },
    ];
    constructor() {}

    ngOnInit(): void {}
}
