import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeCarouselComponent } from './components/home-carousel/home-carousel.component';

@NgModule({
    declarations: [HomeComponent, HomeCarouselComponent],
    imports: [CommonModule, HomeRoutingModule, NgbModule],
})
export class HomeModule {}
