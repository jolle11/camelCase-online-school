import { Component, Input, OnInit } from '@angular/core';
import { ImagesLink } from '../../models/Iimagelinks';

@Component({
    selector: 'app-home-carousel',
    templateUrl: './home-carousel.component.html',
    styleUrls: ['./home-carousel.component.scss'],
})
export class HomeCarouselComponent implements OnInit {
    @Input() images: ImagesLink[] = [];
    constructor() {}

    ngOnInit(): void {}
}
