export interface ImagesLink {
    explore: string;
    meet: string;
    enroll: string;
}
