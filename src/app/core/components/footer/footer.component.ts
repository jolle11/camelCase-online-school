import { Component, OnInit } from '@angular/core';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
    year = new Date().getFullYear(); // Did this to keep the year up to date
    faFacebookF = faFacebookF;
    faInstagram = faInstagram;
    faTwitter = faTwitter;

    constructor() {}

    ngOnInit(): void {}
}
